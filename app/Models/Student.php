<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use HasFactory; use SoftDeletes;

    protected $fillable =['lastname','firstname',  'old' , 'phone', 'email','is_repeater','IdLevel','IdCommune' ];



}
