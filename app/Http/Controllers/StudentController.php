<?php

namespace App\Http\Controllers;
use App\Models\Student;
use App\Models\Level;
use App\Models\Street;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $liste = Student::all();
         $streets =Street::all();
         $classroom = Level::get();
        return view('templates.street_create',compact('liste','streets','classroom'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //formulaire de resgiter
        $liste = Student::all();
        $streets =Street::all();
        $classroom = Level::get();
       return view('templates.street_create',compact('liste','streets','classroom'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // stockons les informations

        $validator = Validator::make($request->all(), [
            'lastname' => 'required|',
            'firstname' => 'required',
            'old' => 'required',
            'phone' => 'required',
            'email' => 'required|string|email|unique:students|max:255',
            'is_repeater' => 'required',
            'IdLevel' => 'required',
            'IdCommune' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('/street/register')
                        ->withErrors($validator)
                        ->withInput();
        }

        $user = Student::create([
            'lastname' => $request->lastname,
            'firstname' => $request->firstname,
            'old' => $request->old,
            'phone' => $request->phone,
            'email' => $request->email,
            'is_repeater' => $request->is_repeater,
            'IdLevel' => $request->IdLevel,
            'IdCommune' => $request->IdCommune,

         ]);

         //var_dump($user);

         return redirect('/street/register')->with('success','Student Enregistré avec succès');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // eleve via sin identifiant
        $eleve = Student::find($id);
        if (is_null($eleve)) {
            return response("echoue");
        }
        return response("reussir avec succès");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $eleve = Student::findOrFail($id);
        return view('template.editStudent',compact('eleve'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        //
        $validator = Validator::make($request->all(),[
            'lastname' => 'required|',
            'firstname' => 'required',
            'old' => 'required',
            'phone' => 'required',
            'email' => 'required|string|email|unique:students|max:255',
            'is_repeater' => 'required',
            'IdLevel' => 'required',

        ]);

        if($validator->fails()){
            return response()->json($validator->errors());
        }

        $student->lastname = $request->name;
        $student->firstname = $request->desc;
        $student->old = $request->old;
        $student->phone = $request->phone;
        $student->email = $request->email;
        $student->is_repeater = $request->is_repeater;
        $student->IdLevel = $request->IdLevel;
        $student->save();

        return redirect('/students/edit/')->with('success','Student modifié avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $eleve = Student::find($id);
        $eleve->delete();
        return redirect('/student/delete')->with('success', 'Student supprimer avec succèss');

    }
}
