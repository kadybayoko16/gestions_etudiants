<?php

namespace App\Http\Controllers;

use App\Models\Level;
use App\Models\Street;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StreetController extends Controller
{




  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $streets =Street::all();
        $classroom = Level::get();
        return view('templates.street_create',compact('streets','classroom'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //formulaire de resgiter

        return view('templates.street_store');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // stockons les informations

        $validator = Validator::make($request->all(), [
            'name' => 'required|',
            'IdStudent' => 'required',


        ]);

        if ($validator->fails()) {
            return redirect('/level/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $streets= Street::create([
            'name' => $request->name,
            'IdStudent' => $request->IdStudent,

         ]);

         return redirect('/level/store/')->with('success','level Enregistré avec succès');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // eleve via sin identifiant
        $streets =Street::findfindOrFail($id);
        if (is_null($streets)) {
            return response("echoue");
        }
        return response("reussir avec succès");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $streets = Street::findOrFail($id);
        return view('template.editStudent',compact('streets'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Street $commune)
    {
        //
        $validator = Validator::make($request->all(),[
            'name' => 'required|',
            'IdStudent' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors());
        }



        $commune->classe = $request->classe;
        $commune->school = $request->school;
        $commune->NameTeacher = $request->NameTeacher;


        return redirect('/students/edit/')->with('success','Level modifié avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $classroom = Street::find($id);
        $classroom->delete();
        return redirect('/level/delete')->with('success', 'Level supprimer avec succèss');

    }
}

