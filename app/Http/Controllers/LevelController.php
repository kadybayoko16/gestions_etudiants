<?php

namespace App\Http\Controllers;

use App\Models\Level;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LevelController extends Controller
{




  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $classroom =Level::get();
        return view('templates.street_create',compact('classroom'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //formulaire de resgiter

        return view('templates.street_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // stockons les informations

        $validator = Validator::make($request->all(), [
            'classe' => 'required|',
            'school' => 'required',
            'NameTeacher' => 'required',

        ]);

        if ($validator->fails()) {
            return redirect('/level/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $class= Level::create([
            'classe' => $request->classe,
            'school' => $request->school,
            'NameTeacher' => $request->NameTeacher,


         ]);

         return redirect('/level/store/')->with('success','level Enregistré avec succès');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // eleve via sin identifiant
        $classroom =Level::findfindOrFail($id);
        if (is_null($classroom)) {
            return response("echoue");
        }
        return response("reussir avec succès");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $classroom = Level::findOrFail($id);
        return view('template.editStudent',compact('classroom'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Level $level)
    {
        //
        $validator = Validator::make($request->all(),[
            'classe' => 'required|',
            'school' => 'required',
            'NameTeacher' => 'required',

        ]);

        if($validator->fails()){
            return response()->json($validator->errors());
        }



        $level->classe = $request->classe;
        $level->school = $request->school;
        $level->NameTeacher = $request->NameTeacher;


        return redirect('/students/edit/')->with('success','Level modifié avec succès');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $classroom = Level::find($id);
        $classroom->delete();
        return redirect('/level/delete')->with('success', 'Level supprimer avec succèss');

    }
}


