<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LevelController;
use App\Http\Controllers\StreetController;
use App\Http\Controllers\StudentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/accueil', function () {
    return view('templates.home');
});

// Route::get('/al', function () {
//     return view('templates.student_create');
// });

Route::get('/street/',[StreetController::class,'index'])->name('commmune');
Route::get('/liste',[LevelController::class,'index'])->name('niveau');
Route::get('/street/register',[StudentController::class,'create'])->name('create.register');
Route::post('/street/register',[StudentController::class,'store'])->name('save');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
