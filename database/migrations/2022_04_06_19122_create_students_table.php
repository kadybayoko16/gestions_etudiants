<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string('lastname');
            $table->string('firstname');
            $table->integer('old');
            $table->string('phone');
            $table->string('email')->nullable();
            $table->boolean('is_repeater')->default(false);
            $table->foreignId('IdLevel')
            ->constrained('levels')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->foreignId('IdCommune')
            ->constrained('streets')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
};
